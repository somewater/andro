package com.example.myapplication2.app.moon;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.example.myapplication2.app.R;

public class HelloMoonFragment extends Fragment {
    private static String PLAYER_POS = "playerPos";
    private static String PLAYER_STATE = "playerState";

    private Button playButton;
    private Button stopButton;
    private AudioPlayer player;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int pos = 0;
        boolean played = false;
        if (savedInstanceState != null) {
            pos = savedInstanceState.getInt(PLAYER_POS, 0);
            played = savedInstanceState.getBoolean(PLAYER_STATE, false);
        }
        player = new AudioPlayer(pos, played);
        //setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hello_moon, container, false);
        playButton = (Button) v.findViewById(R.id.hellomoon_playButton);
        stopButton = (Button) v.findViewById(R.id.hellomoon_stopButton);
        final SurfaceView display = (SurfaceView) v.findViewById(R.id.hellomoon_display);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.play(getActivity(), display.getHolder());
            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.stop();
            }
        });

        if (player.isPlayed())
            player.play(getActivity(), display.getHolder());

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        player.stop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PLAYER_POS, player.getCurrentPos());
        outState.putBoolean(PLAYER_STATE, player.isPlayed());
    }
}
