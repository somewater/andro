package com.example.myapplication2.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends Activity {

    public static final String EXTRA_ANSWER_IS_TRUE = "com.bignerdranch.android.geoquiz.answer_is_true";
    public static final String EXTRA_ANSWER_SHOWN = "com.bignerdranch.android.geoquiz.answer_shown";
    public static final String IS_ANSWER_SHOWN = "isAnswerShown";

    private boolean answerIsTrue;
    private Button answerButton;
    private TextView answerTextView;
    private boolean isAnswerShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        answerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);
        answerButton = (Button)findViewById(R.id.showAnswerButton);
        answerTextView = (TextView)findViewById(R.id.answerTextView);
        answerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answerIsTrue)
                    answerTextView.setText(R.string.true_button);
                else
                    answerTextView.setText(R.string.false_button);
                setAnswerShownResult(true);
            }
        });
        if (savedInstanceState != null)
            isAnswerShown = savedInstanceState.getBoolean(IS_ANSWER_SHOWN, false);
        setAnswerShownResult(isAnswerShown);

        ((TextView)findViewById(R.id.version_number)).setText("SDK #" + Build.VERSION.SDK_INT);
    }

    private void setAnswerShownResult(boolean isAnswerShown) {
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);
        this.isAnswerShown = isAnswerShown;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_ANSWER_SHOWN, isAnswerShown);
    }
}
