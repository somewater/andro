package com.example.myapplication2.app.crime;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.example.myapplication2.app.crime.CrimeFragment;
import com.example.myapplication2.app.crime.SingleFragmentActivity;

import java.util.UUID;

public class CrimeActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return CrimeFragment.newInstance((UUID) getIntent().getSerializableExtra(CrimeFragment.EXTRS_CRIME_ID));
    }
}
