package com.example.myapplication2.app.crime;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.ProgressBar;
import com.example.myapplication2.app.R;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class CrimeCameraFragment extends Fragment implements OrientationManager.OrientationListener {
    private static final String TAG = "CrimeCameraFragment";
    public static final String EXTRA_PHOTO_FILENAME = "com.example.myapplication2.app.crime.EXTRA_PHOTO_FILENAME";
    public static final String EXTRA_ORIENTATION = "com.example.myapplication2.app.crime.EXTRA_ORIENTATION";

    private Camera camera;
    private SurfaceView surfaceView;
    private View progressBar;
    private int orientation = Configuration.ORIENTATION_LANDSCAPE;
    private OrientationManager orientationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressWarnings("deprecation")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crime_camera, container, false);
        Button takePictureButton = (Button) v.findViewById(R.id.crime_camera_takePictureButton);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTakeButtonClick();
            }
        });
        surfaceView = (SurfaceView) v.findViewById(R.id.crime_camera_surfaceView);
        SurfaceHolder holder = surfaceView.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (camera != null)
                        camera.setPreviewDisplay(holder);
                } catch (IOException e) {
                    Log.e(TAG, "Error setting up preview display", e);
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (camera != null) {
                    Camera.Parameters params = camera.getParameters();
                    Camera.Size s = getBestSupportedSize(params.getSupportedPreviewSizes(), width, height);
                    params.setPreviewSize(s.width, s.height);
                    s = getBestSupportedSize(params.getSupportedPictureSizes(), width, height);
                    params.setPictureSize(s.width, s.height);
                    camera.setParameters(params);
                    camera.startPreview();
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (camera != null)
                    camera.stopPreview();
            }
        });

        progressBar = v.findViewById(R.id.crime_camera_progressBarContainer);
        progressBar.setVisibility(View.INVISIBLE);

        orientationManager = new OrientationManager(getActivity(), SensorManager.SENSOR_DELAY_NORMAL, this);
        orientationManager.enable();

        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i++ < 60) {
                    Log.e("ORIENT", "ORINETATION " + getScreenOrientation());
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        return v;
    }

    @Override
    public void onOrientationChange(OrientationManager.ScreenOrientation screenOrientation) {
        switch (screenOrientation) {
            case PORTRAIT:
            case REVERSED_PORTRAIT:
                // swap when natural device orientation is landscape
                orientation = Configuration.ORIENTATION_LANDSCAPE;
                break;
            case LANDSCAPE:
            case REVERSED_LANDSCAPE:
                orientation = Configuration.ORIENTATION_PORTRAIT;
                break;
        }
    }

    private void onTakeButtonClick() {
        Camera.ShutterCallback sc = new Camera.ShutterCallback() {
            @Override
            public void onShutter() {
                progressBar.setVisibility(View.VISIBLE);
            }
        };
        Camera.PictureCallback pc = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                String filename = UUID.randomUUID().toString() + ".jpg";
                FileOutputStream out = null;
                boolean success = true;
                try {
                    out = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
                    out.write(data);
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "Error file creation", e);
                    success = false;
                } catch (IOException e) {
                    Log.e(TAG, "Error file writing", e);
                    success = false;
                } finally {
                    try {
                        if (out != null)
                            out.close();
                    } catch (IOException e) {
                        Log.e(TAG, "Error file closing", e);
                        success = false;
                    }
                }

                if (success) {
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_PHOTO_FILENAME, filename);
                    intent.putExtra(EXTRA_ORIENTATION, getScreenOrientation());
                    getActivity().setResult(Activity.RESULT_OK, intent);
                } else {
                    getActivity().setResult(Activity.RESULT_CANCELED);
                }
                getActivity().finish();
            }
        };
        camera.takePicture(sc, null, pc);
    }

    public int getScreenOrientation()
    {
        return orientation;
    }

    @TargetApi(9)
    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            camera = Camera.open(0);
        } else {
            camera = Camera.open();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }

    private Camera.Size getBestSupportedSize(List<Camera.Size> sizes, int width, int height) {
        Camera.Size best = sizes.get(0);
        int bestArea = best.width * best.height;
        for (Camera.Size s : sizes) {
            int area = s.width * s.height;
            if (area > bestArea) {
                best = s;
                bestArea = area;
            }
        }
        return best;
    }
}
