package com.example.myapplication2.app;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class QuizActivity extends Activity {

    private static String TAG = "QuizActivity";
    private static String KEY_INDEX = "index";
    private static final String KEY_IS_CHEATER = "isCheater";

    private Button trueButton;
    private Button falseButton;
    private Button cheatButton;
    private ImageButton nextButton;
    private ImageButton prevButton;
    private TextView questionText;
    private int curIndex = 0;
    private boolean isCheater = false;

    @TargetApi(11)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        trueButton = (Button) findViewById(R.id.true_button);
        falseButton = (Button) findViewById(R.id.false_button);
        nextButton = (ImageButton) findViewById(R.id.next_button);
        prevButton = (ImageButton) findViewById(R.id.prev_button);
        questionText = (TextView) findViewById(R.id.question_text);
        cheatButton = (Button) findViewById(R.id.cheat_button);

        trueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAnswer(true);
            }
        });

        falseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAnswer(false);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextQuestion();
            }
        });
        questionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextQuestion();
            }
        });
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prevQuestion();
            }
        });
        cheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCheat();
            }
        });

        if (savedInstanceState != null) {
            curIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            isCheater = savedInstanceState.getBoolean(KEY_IS_CHEATER, false);
        }
        update();
        Log.d(TAG, "OnCreate");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar actionBar = getActionBar();
            actionBar.setSubtitle("Bodies of Water");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null)
            return;
        isCheater = data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN, false);
    }

    private void onCheat() {
        startActivityForResult(new Intent(this, CheatActivity.class).
                putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE, questionBank()[curIndex].isTrueAnswer()), 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt(KEY_INDEX, curIndex);
        state.putBoolean(KEY_IS_CHEATER, isCheater);
        Log.d(TAG, "OnSaveInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "OnStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "OnStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "OnPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "OnDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public TrueFalse[] questionBank() {
        return new TrueFalse[] {
                new TrueFalse(R.string.question_oceans, true),
                new TrueFalse(R.string.question_mideast, false),
                new TrueFalse(R.string.question_africa, false),
                new TrueFalse(R.string.question_americas, true),
                new TrueFalse(R.string.question_asia, true)
        };
    }

    private void onAnswer(boolean answer) {
        boolean correct = questionBank()[curIndex].isTrueAnswer() == answer;
        int toastText;
        if (isCheater)
            toastText = R.string.judgment_toast;
        else if (correct)
            toastText = R.string.correct_toast;
        else
            toastText = R.string.incorrect_toast;
        Toast.makeText(QuizActivity.this, toastText, Toast.LENGTH_SHORT).show();

        nextQuestion();
    }

    private void nextQuestion() {
        curIndex = (curIndex + 1) % questionBank().length;
        update();
        isCheater = false;
    }

    private void prevQuestion() {
        curIndex = (curIndex - 1) % questionBank().length;
        update();
        isCheater = false;
    }

    private void update() {
        questionText.setText(questionBank()[curIndex].getQuestion());
        prevButton.setVisibility(curIndex > 0 ? View.VISIBLE : View.INVISIBLE);
    }
}
