package com.example.myapplication2.app.crime;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import java.util.Date;

public class TimeOrDateDialogFragment extends DialogFragment {

    public static final int RESULT_DATE = 1;
    public static final int RESULT_TIME = 2;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle("Change date or time?")
                .setPositiveButton("Date", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_DATE, null);
                    }
                })
                .setNeutralButton("Time", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_TIME, null);
                    }
                })
                .create();
    }
}
