package com.example.myapplication2.app.crime;

import android.content.Context;
import android.util.Log;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class CrimeLab {
    private static final String TAG = "CrimeLab";
    private static CrimeLab instance;
    private Context appContext;
    private CriminalIntentJSONSerializer serializer;
    private ArrayList<Crime> crimes;

    public CrimeLab(Context c) {
        appContext = c;
        serializer = new CriminalIntentJSONSerializer(c, "crimes.json");
        try {
            crimes = serializer.loadCrimes();
        } catch (Exception e) {
            e.printStackTrace();
            crimes = new ArrayList<>();
            Log.e(TAG, "Error loading crimes: ", e);
        }
    }

    public static CrimeLab get(Context c) {
        if (instance == null)
            instance = new CrimeLab(c.getApplicationContext());
        instance.appContext = c;
        return instance;
    }

    public ArrayList<Crime> getCrimes() {
        return crimes;
    }

    public void addCrime(Crime crime) {
        crimes.add(crime);
    }

    public void deleteCrime(Crime c) {
        crimes.remove(c);
    }

    public Crime getCrime(UUID id) {
        for (Crime c : crimes)
            if (c.getId().equals(id))
                return c;
        return null;
    }

    public boolean saveCrimes() {
        try {
            serializer.saveCrimes(crimes);
            Log.d(TAG, "crimes saved in file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving crimes: ", e);
            return false;
        }

    }
}
