package com.example.myapplication2.app.crime;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.*;
import android.widget.*;
import com.example.myapplication2.app.R;

import java.io.File;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CrimeFragment extends Fragment {
    public static final String EXTRS_CRIME_ID = "extraCrimeId";
    private static final String DIALOG_DATE = "date";
    public static final int REQUEST_DATE = 0;
    public static final int REQUEST_CHOOSE_DAY_OR_TIME = 1;
    public static final int REQUEST_PHOTO = 2;
    public static final int REQUEST_CONTACT = 3;
    private static final String DIALOG_IMAGE = "image";
    private static final int MENU_DELETE = 1;

    private Crime crime;
    private EditText titleField;
    private Button dateButton;
    private CheckBox solvedCheckBox;
    private ImageView photoView;
    private ImageButton photoButton;
    private Button suspectButton;
    private Button sendReportButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID crimeId = (UUID) getArguments().getSerializable(EXTRS_CRIME_ID);
        this.crime = CrimeLab.get(getActivity()).getCrime(crimeId);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        showPhoto();
    }

    @Override
    public void onStop() {
        super.onStop();
        PictureUtils.cleanImageView(photoView);
    }

    @Override
    public void onPause() {
        super.onPause();
        CrimeLab.get(getActivity()).saveCrimes();
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crime, container, false);
        titleField = (EditText) v.findViewById(R.id.crime_title);
        dateButton = (Button) v.findViewById(R.id.crime_data);
        solvedCheckBox = (CheckBox) v.findViewById(R.id.crime_solved);

        titleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                crime.setTitle(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                TimeOrDateDialogFragment dialog = new TimeOrDateDialogFragment();
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_CHOOSE_DAY_OR_TIME);
                dialog.show(fm, DIALOG_DATE);
            }
        });
        titleField.setText(crime.getTitle());
        dateButton.setText(dateToString(crime.getDate()));
        //dateButton.setEnabled(false);
        solvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                crime.setSolved(b);
            }
        });
        solvedCheckBox.setChecked(crime.isSolved());
        photoButton = (ImageButton) v.findViewById(R.id.crime_photoButton);
        photoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CrimeCameraActivity.class);
                startActivityForResult(intent, REQUEST_PHOTO);
            }
        });
        photoView = (ImageView) v.findViewById(R.id.crime_imageView);
        photoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Photo p = crime.getPhoto();
                if (p != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    String path = getActivity().getFileStreamPath(p.getFilename()).getAbsolutePath();
                    ImageFragment.newInstance(path, p.getOrientation()).show(fm, DIALOG_IMAGE);
                }
            }
        });
        PackageManager pm = getActivity().getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) && !pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            photoButton.setEnabled(false);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && NavUtils.getParentActivityName(getActivity()) != null) {
            getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        suspectButton = (Button) v.findViewById(R.id.crime_suspectButton);
        suspectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                PackageManager pm = getActivity().getPackageManager();
                List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);
                if (activities.size() > 0)
                    startActivityForResult(intent, REQUEST_CONTACT);
            }
        });
        updateSuspect();
        sendReportButton = (Button) v.findViewById(R.id.crime_reportButton);
        sendReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, getCrimeReport());
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.crime_report_subject));
                intent = Intent.createChooser(intent, getString(R.string.send_report));
                startActivity(intent);
            }
        });
        return v;
    }

    private void updateSuspect() {
        if (crime.getSuspect() != null)
            suspectButton.setText(crime.getSuspect());
        else
            suspectButton.setText(R.string.crime_suspect_text);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime, menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        switch (v.getId()) {
            case R.id.crime_imageView:
                menu.add(0, MENU_DELETE, 0, R.string.delete_photo);
                break;
            default:
                super.onCreateContextMenu(menu, v, menuInfo);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_DELETE:
                crime.setPhoto(null);
                showPhoto();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (NavUtils.getParentActivityName(getActivity()) != null) {
                    NavUtils.navigateUpFromSameTask(getActivity());
                }
                return true;
            case R.id.menu_item_delete_crime:
                CrimeLab.get(getActivity()).deleteCrime(crime);
                if (NavUtils.getParentActivityName(getActivity()) != null) {
                    NavUtils.navigateUpFromSameTask(getActivity());
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_DATE:
                Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
                if (date == null)
                    date = (Date) data.getSerializableExtra(TimePickerFragment.EXTRA_TIME);
                crime.setDate(date);
                dateButton.setText(dateToString(crime.getDate()));
                break;
            case REQUEST_CHOOSE_DAY_OR_TIME:
                System.out.println();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                DialogFragment dialog;
                if (resultCode == TimeOrDateDialogFragment.RESULT_DATE)
                    dialog = DatePickerFragment.newInstance(crime.getDate());
                else if (resultCode == TimeOrDateDialogFragment.RESULT_TIME)
                    dialog = TimePickerFragment.newInstance(crime.getDate());
                else
                    throw new RuntimeException("Undefined result " + resultCode);
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            case REQUEST_PHOTO:
                if (data ==  null) return;
                String filename = data.getStringExtra(CrimeCameraFragment.EXTRA_PHOTO_FILENAME);
                int orientation = data.getIntExtra(CrimeCameraFragment.EXTRA_ORIENTATION, Configuration.ORIENTATION_LANDSCAPE);
                if (filename != null) {
                    if (crime.getPhoto() != null)
                        safeDeleteFile(crime.getPhoto().getFilename());
                    Photo photo = new Photo(filename, orientation);
                    crime.setPhoto(photo);
                    showPhoto();
                }
                break;
            case REQUEST_CONTACT:
                if (data == null) return;
                Uri contactUri = data.getData();
                String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
                Cursor cursor = getActivity().getContentResolver()
                        .query(contactUri, projection, null, null, null);

                if (cursor.getCount() == 0) {
                    cursor.close();
                    return;
                }

                cursor.moveToFirst();
                String suspect = cursor.getString(Arrays.asList(projection).indexOf(ContactsContract.Contacts.DISPLAY_NAME));
                crime.setSuspect(suspect);
                updateSuspect();
                cursor.close();
                break;
        }
    }

    private void safeDeleteFile(String filename) {
        if (filename != null && filename.length() > 0) {
            String path = getActivity().getFileStreamPath(filename).getAbsolutePath();
            File file = new File(path);
            if (file.exists())
                file.delete();
        }
    }

    private String dateToString(Date date) {
        DateFormat f = new SimpleDateFormat("c, MMM dd, yyyy");
        return f.format(date);
    }

    public static CrimeFragment newInstance(UUID crimeId) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRS_CRIME_ID, crimeId);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void showPhoto() {
        Photo p = crime.getPhoto();
        if (p != null) {
            String path = getActivity().getFileStreamPath(p.getFilename()).getAbsolutePath();
            PictureUtils.putScaledDrawable(getActivity(), photoView, path, p.getOrientation());
            registerForContextMenu(photoView);
        } else {
            photoView.setImageDrawable(null);
            unregisterForContextMenu(photoView);
        }
    }

    private String getCrimeReport() {
        String solvedString = null;
        if (crime.isSolved()) {
            solvedString = getString(R.string.crime_report_solved);
        } else {
            solvedString = getString(R.string.crime_report_unsolved);
        }
        String dateFormat = "EEE, MMM dd";
        String dateString = android.text.format.DateFormat.format(dateFormat, crime.getDate()).toString();
        String suspect = crime.getSuspect();
        if (suspect == null) {
            suspect = getString(R.string.crime_report_no_suspect);
        } else {
            suspect = getString(R.string.crime_report_suspect, suspect);
        }
        String report = getString(R.string.crime_report,
                crime.getTitle(), dateString, solvedString, suspect);
        return report;
    }

    private void phoneCall(String phone) {
        Uri number = Uri.parse("tel:" + phone);
        Intent intent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(intent);
    }
}
