package com.example.myapplication2.app.crime;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.Display;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

public class PictureUtils {
    public static BitmapDrawable getScaledDrawable(Activity a, String path, int orientation) {
        boolean needRotate = orientation != Configuration.ORIENTATION_LANDSCAPE;
        Display display = a.getWindowManager().getDefaultDisplay();
        Point p = new Point();
        if (Build.VERSION.SDK_INT > 13) {
            display.getSize(p);
        } else {
            p.set(display.getWidth(), display.getHeight());
        }

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opts);

        int srcWidth = opts.outWidth;
        int srcHeight = opts.outHeight;
        if (needRotate) {
            int tmp = srcWidth; srcWidth = srcHeight; srcHeight = tmp;
        }

        int inSampleSize = 1;
        if (srcHeight > p.y || srcWidth > p.x) {
            if (srcWidth > srcHeight) {
                inSampleSize = Math.round(srcHeight / p.y);
            } else {
                inSampleSize = Math.round(srcWidth / p.x);
            }
        }

        opts = new BitmapFactory.Options();
        opts.inSampleSize = inSampleSize;
        Bitmap bitmap = BitmapFactory.decodeFile(path, opts);
        return new BitmapDrawable(a.getResources(), bitmap);
    }
    
    public static void putScaledDrawable(Activity a, final ImageView imageView, String path, int orientation) {
        final BitmapDrawable bmp = getScaledDrawable(a, path, orientation);
        imageView.setImageDrawable(bmp);
        boolean needRotate = orientation != Configuration.ORIENTATION_LANDSCAPE;
        if (needRotate) {
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
            imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Matrix m = new Matrix();
                    float scale = Math.min((float) imageView.getWidth() / bmp.getBitmap().getHeight(), (float) imageView.getHeight() / bmp.getBitmap().getWidth());
                    float scaledBmpWidth = bmp.getBitmap().getWidth() * scale;
                    float scaledBmpHeight = bmp.getBitmap().getHeight() * scale;
                    m.setScale(scale, scale);
                    m.postTranslate(
                            (imageView.getWidth() - scaledBmpWidth) * 0.5f,
                            (imageView.getHeight() - scaledBmpHeight) * 0.5f);
                    m.postRotate(90, imageView.getWidth() * 0.5f, imageView.getHeight() * 0.5f);
                    imageView.setImageMatrix(m);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                        imageView.setCropToPadding(true);

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                        imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    else
                        imageView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            });
        } else {
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
    }

    public static void cleanImageView(ImageView imageView) {
        if (!(imageView.getDrawable() instanceof BitmapDrawable))
            return;

        BitmapDrawable b = (BitmapDrawable) imageView.getDrawable();
        b.getBitmap().recycle();
        imageView.setImageBitmap(null);
    }
}
