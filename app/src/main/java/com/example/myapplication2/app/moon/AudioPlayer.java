package com.example.myapplication2.app.moon;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.view.SurfaceHolder;
import com.example.myapplication2.app.R;

public class AudioPlayer {
    private MediaPlayer mPlayer;
    private int currentPos;
    private boolean played;

    public AudioPlayer(int currentPos, boolean played) {
        this.currentPos = currentPos;
        this.played = played;
    }

    public void stop() {
        if (mPlayer != null) {
            currentPos = mPlayer.getCurrentPosition();
            mPlayer.release();
            mPlayer = null;
            played = false;
        }
    }

    public void play(Context c, @Nullable SurfaceHolder display) {
        stop();

        if (display == null)
            mPlayer = MediaPlayer.create(c, R.raw.one_small_step);
        else
            mPlayer = MediaPlayer.create(c, R.raw.one_small_step);

        if (display != null)
            mPlayer.setDisplay(display);
        if (currentPos > 0)
            mPlayer.seekTo(currentPos);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stop();
                currentPos = 0;
            }
        });
        mPlayer.start();
        played = true;
    }

    public int getCurrentPos() {
        if (isPlayed())
            return mPlayer.getCurrentPosition();
        else
            return currentPos;
    }

    public boolean isPlayed() {
        return played;
    }
}
