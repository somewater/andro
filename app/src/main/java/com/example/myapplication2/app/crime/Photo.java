package com.example.myapplication2.app.crime;

import android.content.res.Configuration;
import org.json.JSONException;
import org.json.JSONObject;

public class Photo {
    public static final String JSON_FILENAME = "filename";
    public static final String JSON_ORIENTATION = "orientation";

    private String filename;
    private int orientation = Configuration.ORIENTATION_LANDSCAPE;

    public Photo(String filename, int orientation) {
        this.filename = filename; this.orientation = orientation;
    }

    public Photo(JSONObject json) throws JSONException {
        filename = json.getString(JSON_FILENAME);
        orientation = json.getInt(JSON_ORIENTATION);
    }

    public String getFilename() {
        return filename;
    }

    public int getOrientation() {
        return orientation;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_FILENAME, filename);
        json.put(JSON_ORIENTATION, orientation);
        return json;
    }
}
