package com.example.myapplication2.app;

public class TrueFalse {
    private int question;
    private boolean trueAnswer;

    public TrueFalse(int question, boolean trueAnswer) {
        this.question = question;
        this.trueAnswer = trueAnswer;
    }

    public int getQuestion() {
        return question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

    public boolean isTrueAnswer() {
        return trueAnswer;
    }

    public void setTrueAnswer(boolean trueAnswer) {
        this.trueAnswer = trueAnswer;
    }
}
