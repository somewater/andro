package com.example.myapplication2.app.crime;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TimePicker;
import com.example.myapplication2.app.R;

import java.util.Calendar;
import java.util.Date;

public class TimePickerFragment extends DialogFragment {
    public static String EXTRA_TIME = "com.bignerdranch.android.criminalintent.time";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Date date = (Date) getArguments().getSerializable(EXTRA_TIME);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int min = calendar.get(Calendar.MINUTE);


        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_time, null);
        TimePicker tp = (TimePicker) v.findViewById(R.id.dialog_time_timePicker);
        tp.setCurrentHour(hour);
        tp.setCurrentMinute(min);
        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                updateDateFromArgs(view.getCurrentHour(), view.getCurrentMinute());
            }
        });

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(R.string.time_picker_title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendResults(Activity.RESULT_OK);
                    }
                })
                .create();
    }

    public static TimePickerFragment newInstance(Date date) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_TIME, date);
        TimePickerFragment tp = new TimePickerFragment();
        tp.setArguments(args);
        return tp;
    }

    private void sendResults(int resultCode) {
        if (getTargetFragment() == null)
            return;

        Intent i = new Intent();
        i.putExtra(EXTRA_TIME, getArguments().getSerializable(EXTRA_TIME));
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);
    }

    private void updateDateFromArgs(int newHour, int newMin) {
        Date date = (Date) getArguments().getSerializable(EXTRA_TIME);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR, newHour);
        c.set(Calendar.MINUTE, newMin);
        getArguments().putSerializable(EXTRA_TIME, c.getTime());
    }
}
