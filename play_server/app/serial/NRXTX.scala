package serial
import java.util

import gnu.io._
import java.io._

object NRXTX extends NRXTX {

  val DEFAULT_PORT = "COM3"
  var started = false

  def start: Unit =
    if(serialPort == null) {
      start(DEFAULT_PORT)
      started = true
    }

  def setPin(pin: Int): Unit = {
    if (serialPort == null)
      start(DEFAULT_PORT)
    writeData(s"pin:$pin")
  }

  override def finalize(): Unit = {
    super.finalize()
    stop()
  }
}

class NRXTX extends SerialPortEventListener {

  protected var input: BufferedReader = _
  protected var output: OutputStream = _

  protected var serialPort: SerialPort = _

  def start(portName: String) {
    val portIdentifier: CommPortIdentifier = CommPortIdentifier.getPortIdentifier(portName)
    if (portIdentifier.isCurrentlyOwned)
      throw new RuntimeException("Error: Port is currently in use")
    val commPort: CommPort = portIdentifier.open(this.getClass.getName, 2000)
    if(!commPort.isInstanceOf[SerialPort])
      throw new RuntimeException("Error: Only serial ports are handled by this example.")
    serialPort = commPort.asInstanceOf[SerialPort]
    serialPort.addEventListener(this)
    serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE)


    input = new BufferedReader(new InputStreamReader(serialPort.getInputStream, "US-ASCII"))
    output = serialPort.getOutputStream
    serialPort.notifyOnDataAvailable(true)
  }

  override def serialEvent(serialPortEvent: SerialPortEvent): Unit = {
    if (serialPortEvent.getEventType == SerialPortEvent.DATA_AVAILABLE) {
      val msg = input.readLine()
      onMsg(msg)
      val cmdEnd = msg.indexOf(':')
      if (cmdEnd != -1) {
        onCmd(msg.substring(0, cmdEnd), msg.substring(cmdEnd + 1))
      }
    }
  }

  def onCmd(cmd: String, data: String) = {

  }

  def onMsg(msg: String): Unit = {

  }

  def writeData(data: String) {
    output.write(data.getBytes("US-ASCII"));
  }

  def stop(): Unit = {
    if (serialPort != null) {
      input = null;
      output = null;
      serialPort.removeEventListener();
      serialPort.close();
      serialPort = null;
    }
  }
}