package controllers

import play.api._
import play.api.libs.iteratee.{Concurrent, Enumerator, Iteratee}
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits._
import serial.NRXTX

object Application extends Controller {

  def index = Action { implicit request =>
    Ok(views.html.ws(request))
  }

  def ws = WebSocket.using[String] { implicit request =>
    val (out,channel) = Concurrent.broadcast[String]
    val in = Iteratee.foreach[String] {
      msg =>
        NRXTX.writeData(msg)
        channel.push("CMD: " + msg)
    }
    (in,out)
  }
}