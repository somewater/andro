import java.io.File

import play.{Application, Mode, Configuration, GlobalSettings}
import serial.NRXTX

class Global extends GlobalSettings {
  override def onStart(app: Application): Unit = {
    super.onStart(app)
    NRXTX.start
  }

  override def onStop(app: Application): Unit = {
    super.onStop(app)
    NRXTX.stop()
  }
}
